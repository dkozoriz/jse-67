<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/_header.jsp"/>
<h1>Project edit</h1>

<form:form action="/task/edit/${task.id}/" method="POST" modelAttribute="task">
    <form:input type="hidden" path="id"/>

    <p>
    <div>Name:</div>
    <div><form:input type="text" path="name"/></div>


    <p>
    <div>Description:</div>
    <div><form:input type="text" path="description"/></div>

    <p>
    <div>Status:</div>
    <div>
        <form:select path="status">
            <form:option value="${null}" label="-"/>
            <form:options items="${statuses}" itemLabel="displayName"/>
        </form:select>
    </div>

    <p>
    <div>Project:</div>
    <div>
        <form:select path="projectId">
            <form:option value="${null}" label="-"/>
            <form:options items="${projects}" itemLabel="name" itemValue="id"/>
        </form:select>
    </div>

    <p>
    <button type="submit">Save task</button>
</form:form>

<jsp:include page="../include/_footer.jsp"/>