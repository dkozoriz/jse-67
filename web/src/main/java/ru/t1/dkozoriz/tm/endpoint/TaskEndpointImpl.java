package ru.t1.dkozoriz.tm.endpoint;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.t1.dkozoriz.tm.api.TaskEndpoint;
import ru.t1.dkozoriz.tm.model.Task;
import ru.t1.dkozoriz.tm.service.TaskService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/task")
@WebService(endpointInterface = "ru.t1.dkozoriz.tm.api.TaskEndpoint")
public class TaskEndpointImpl implements TaskEndpoint {

    private final TaskService taskService;

    @Override
    @GetMapping("/getAll")
    @WebMethod
    public List<Task> findAll() {
        return taskService.findAll();
    }

    @Override
    @GetMapping("/count")
    @WebMethod
    public Long count() {
        return taskService.count();
    }

    @Override
    @GetMapping("/get/{id}")
    @WebMethod
    public Task findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        return taskService.findById(id);
    }

    @Override
    @PostMapping("/post")
    @WebMethod
    public Task save(
            @WebParam(name = "task", partName = "task")
            @RequestBody Task task
    ) {
        return taskService.save(task);
    }

    @Override
    @PutMapping("/put")
    @WebMethod
    public Task update(
            @WebParam(name = "task", partName = "task")
            @RequestBody Task task
    ) {
        return taskService.update(task);
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete/{id}")
    public void delete(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        taskService.deleteById(id);
    }

    @Override
    @DeleteMapping("/deleteAll")
    @WebMethod
    public void deleteAll() {
        taskService.deleteAll();
    }

}