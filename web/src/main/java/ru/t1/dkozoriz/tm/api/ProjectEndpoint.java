package ru.t1.dkozoriz.tm.api;

import org.springframework.web.bind.annotation.*;
import ru.t1.dkozoriz.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ProjectEndpoint {

    @GetMapping("/getAll")
    @WebMethod
    List<Project> findAll();

    @GetMapping("/count")
    @WebMethod
    Long count();

    @GetMapping("/get/{id}")
    @WebMethod
    Project findById(
            @PathVariable("id") String id
    );

    @PostMapping("/post")
    @WebMethod
    Project save(@RequestBody Project project);

    @PutMapping("/put")
    @WebMethod
    Project update(
            @RequestBody Project project
    );

    @DeleteMapping("/delete/{id}")
    @WebMethod
    void delete(
            @PathVariable("id") String id
    );

    @DeleteMapping("/deleteAll")
    @WebMethod
    void deleteAll();

}