package ru.t1.dkozoriz.tm.api;

import org.springframework.web.bind.annotation.*;
import ru.t1.dkozoriz.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface TaskEndpoint {

    @GetMapping("/findAll")
    @WebMethod
    List<Task> findAll();

    @GetMapping("/count")
    @WebMethod
    Long count();

    @GetMapping("/find/{id}")
    @WebMethod
    Task findById(
            @PathVariable("id") String id
    );

    @WebMethod
    @PostMapping("/save")
    Task save(@RequestBody Task task);

    @WebMethod
    @PutMapping("/update")
    Task update(
            @RequestBody Task task
    );

    @DeleteMapping("/delete/{id}")
    @WebMethod
    void delete(
            @PathVariable("id") String id
    );

    @DeleteMapping("/deleteAll")
    @WebMethod
    void deleteAll();

}