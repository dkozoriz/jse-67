package ru.t1.dkozoriz.tm.endpoint;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.t1.dkozoriz.tm.api.ProjectEndpoint;
import ru.t1.dkozoriz.tm.model.Project;
import ru.t1.dkozoriz.tm.service.ProjectService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/project")
@WebService(endpointInterface = "ru.t1.dkozoriz.tm.api.ProjectEndpoint")
public class ProjectEndpointImpl implements ProjectEndpoint {

    private final ProjectService projectService;

    @Override
    @GetMapping("/getAll")
    @WebMethod
    public List<Project> findAll() {
        return projectService.findAll();
    }

    @Override
    @GetMapping("/count")
    @WebMethod
    public Long count() {
        return projectService.count();
    }

    @Override
    @GetMapping("/get/{id}")
    @WebMethod
    public Project findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        return projectService.findById(id);
    }

    @Override
    @PostMapping("/post")
    @WebMethod
    public Project save(
            @WebParam(name = "project", partName = "project")
            @RequestBody Project project) {
        return projectService.save(project);
    }

    @Override
    @PutMapping("/put")
    @WebMethod
    public Project update(
            @WebParam(name = "project", partName = "project")
            @RequestBody Project project
    ) {
        return projectService.update(project);
    }

    @Override
    @DeleteMapping("/delete/{id}")
    @WebMethod
    public void delete(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        projectService.deleteById(id);
    }

    @Override
    @DeleteMapping("/deleteAll")
    @WebMethod
    public void deleteAll() {
        projectService.deleteAll();
    }

}