package ru.t1.dkozoriz.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.dkozoriz.tm.model.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, String> {

}